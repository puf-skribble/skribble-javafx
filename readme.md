# Anleitung
## Build einrichten
Um die Anwendung zu starten muss eine Run-Konfiguration angelegt werden.
![img.png](img.png)
Diese muss vom Typ "Application" sein und auf die Datei SkribbleApplication verweisen.
Anschließend kann die Anwendung gestartet werden.
module com.skribble {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;
    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires java.desktop;
    requires unirest.java;
    requires java.sql;
    requires javax.websocket.api;
    requires spring.messaging;
    requires spring.websocket;
    requires json.simple;
    requires org.joda.time;
    requires com.fasterxml.jackson.databind;

    opens com.skribble to javafx.fxml;
    exports com.skribble;
    opens com.skribble.api.websocket.actionHandling.inputs to com.fasterxml.jackson.databind;
    opens com.skribble.api.websocket.actionHandling.response to com.fasterxml.jackson.databind;
    opens com.skribble.user to com.fasterxml.jackson.databind;
    opens com.skribble.api.websocket.actionHandling.response.factory to com.fasterxml.jackson.databind;
    opens com.skribble.drawingTools to com.fasterxml.jackson.databind;
    exports com.skribble.utils;
    opens com.skribble.utils to javafx.fxml;
    exports com.skribble.holder;
    opens com.skribble.holder to com.fasterxml.jackson.databind, javafx.fxml;
}
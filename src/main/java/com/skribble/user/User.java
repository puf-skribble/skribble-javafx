package com.skribble.user;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
    @JsonProperty("username")
    private String username;
    @JsonProperty("avatar")
    private Avatar avatar;
    @JsonProperty("host")
    private boolean host;
    private Integer score;
    private String jwt;
    private String jwtExpire;
    private boolean correct;

    private final String AVATAR_BASE = "https://p-frei.de";
    public User() {
        this.score = 0;
    }

    public User(String name, String jwt) {
        this.username = name;
        this.score = 0;
        this.avatar = new Avatar();
        this.jwt = jwt;
    }

    public User(String username, Avatar avatar, boolean host) {
        this.username = username;
        this.avatar = avatar;
        this.host = host;
    }


    public boolean isHost() {
        return host;
    }

    public void setHost(boolean host) {
        this.host = host;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public String getAvatar() {
        return AVATAR_BASE + avatar.getImagePath();
    }

    public int getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getJWT() {return jwt;}

    public void setJWT(String jwt) {this.jwt = jwt;}

    public String getJwtExpire() {
        return jwtExpire;
    }

    public void setJwtExpire(String jwtExpire) {
        this.jwtExpire = jwtExpire;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }
}

package com.skribble.user;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class GameStatistics {
    private long playerGameNumber;
    private String timePlayed;
    private long playersCount;
    private long score;
    private long rank;

    public long getPlayerGameNumber() {
        return playerGameNumber;
    }

    public void setPlayerGameNumber(long playerGameNumber) {
        this.playerGameNumber = playerGameNumber;
    }

    public String getTimePlayed() {
        return timePlayed;
    }

    public void setTimePlayed(String timePlayed) {
        this.timePlayed = timePlayed;
    }

    public long getPlayersCount() {
        return playersCount;
    }

    public void setPlayersCount(long playersCount) {
        this.playersCount = playersCount;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    public String toString() {
        ZonedDateTime date = ZonedDateTime.parse(this.timePlayed);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String text = date.format(formatter);
        return text + " - Rang: " + this.rank + " (Punkte: " + this.score + ") - Gegenspieler: " + this.playersCount;
    }
}

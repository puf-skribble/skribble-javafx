package com.skribble.user;

public class Avatar {

    private String name;
    private String imagePath;

    public Avatar() {
    }

    public Avatar(String name, String imagePath) {
        this.name = name;
        this.imagePath = imagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String toString() {
        return "{\n" +
                "\t\"avatar\": {\n" +
                "        \"name\":" + this.name + ",\n" +
                "        \"imagePath\":" + this.imagePath + ",\n" +
                "    }\n" +
                "}\t";
    }
}

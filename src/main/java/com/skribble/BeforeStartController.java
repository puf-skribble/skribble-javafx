package com.skribble;

import com.skribble.api.websocket.actionHandling.inputs.Action;
import com.skribble.api.websocket.actionHandling.inputs.StartGameAction;
import com.skribble.holder.ClientHolder;
import com.skribble.utils.SceneChanger;
import com.skribble.SkribbleGame;
import com.skribble.holder.SkribbleGameHolder;
import com.skribble.customListview.WaitinglistCellFactory;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import com.skribble.user.User;
import com.skribble.holder.UserHolder;

import java.util.ArrayList;
import java.util.List;

public class BeforeStartController {
    private User currentUser;
    private SkribbleGame game;
    @FXML
    private ListView<User> waitingList;
    @FXML
    private AnchorPane anchor;
    @FXML
    private VBox waitingListContainer;
    @FXML
    protected void initialize() {
        game = SkribbleGameHolder.getInstance().getGame();
        currentUser = UserHolder.getInstance().getUser();
        initStartButton(game.getLeaderboard());
        waitingList.setCellFactory(new WaitinglistCellFactory());
        game.openGameScenePropProperty().addListener((observableValue, aBoolean, t1) -> {
            if (t1 && !currentUser.isHost()) {
                Platform.runLater(() -> SceneChanger.change("skribble-game.fxml", anchor));
            }
        });
        Platform.runLater(() ->  fillWaitingList(game.getLeaderboard()));
        addBackBtn();
    }

    private void addBackBtn() {
        Button btn = new Button();
        btn.setText("Zurück");
        btn.getStyleClass().add("btn");
        btn.getStyleClass().add("btn-danger");

        btn.setOnAction(actionEvent ->SceneChanger.change("home-view.fxml", anchor));
        waitingListContainer.getChildren().add(btn);
    }

    private void fillWaitingList(ObservableList< User> addedSubList) {
        waitingList.setItems(addedSubList);
    }

    private void initStartButton(ObservableList<User> addedSubList) {
        for (User u: addedSubList) {
            if (u.isHost()) {
                setStartButton(u);
            }
        }
    }

    private void setStartButton(User u) {
        if (currentUser.getUsername().equals(u.getUsername())) {
            currentUser.setHost(true);
            Button startButton = new Button();
            startButton.setText("Spiel starten...");
            startButton.getStyleClass().add("btn");
            startButton.getStyleClass().add("btn-info");
            startButton.setOnAction(actionEvent -> startGame());
            waitingListContainer.getChildren().add(startButton);
        } else {
            Text infoText = new Text();
            currentUser.setHost(false);
            infoText.setText("Der Host muss das Spiel starten. Habe noch ein wenig Geduld!");
            waitingListContainer.getChildren().add(infoText);
        }

    }

    private void startGame() {
        SceneChanger.change("skribble-game.fxml", anchor);
    }


}

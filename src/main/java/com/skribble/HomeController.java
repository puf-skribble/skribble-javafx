package com.skribble;

import com.skribble.api.tasks.*;
import com.skribble.api.websocket.SkribbleWebsocketClient;
import com.skribble.holder.ClientHolder;
import com.skribble.utils.SceneChanger;
import com.skribble.holder.SkribbleGameHolder;
import com.skribble.api.websocket.actionHandling.inputs.Action;
import com.skribble.api.websocket.actionHandling.inputs.AddUserToGameAction;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import com.skribble.user.Avatar;
import com.skribble.user.GameStatistics;
import com.skribble.user.User;
import com.skribble.holder.UserHolder;
import org.springframework.messaging.simp.stomp.StompSession;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;


public class HomeController{

    @FXML
    private Button joinGame;
    @FXML
    private Button createGame;
    @FXML
    private TextField gameId;
    @FXML
    private Text newGameId;
    @FXML
    private ImageView profileImg;
    @FXML
    private Button changeImage;
    @FXML
    private Button saveImage;
    @FXML
    private ListView history;
    @FXML
    private Text welcomeText;
    @FXML
    private AnchorPane anchor;
    @FXML
    private Button copyGameId;
    @FXML
    private HBox newGameIdContainer;

    private UserHolder holder;
    private SkribbleWebsocketClient client;
    private User currentUser;
    private ArrayList<Avatar> avatarList;
    private int avatarCount = 0;

    private final String WEBSOCKET_URL =  "wss://p-frei.de/game-websocket-connection";


    @FXML
    protected void initialize() {
        holder = UserHolder.getInstance();
        currentUser = holder.getUser();
        client = new SkribbleWebsocketClient(WEBSOCKET_URL, currentUser.getJWT());
        client.subscriptions.forEach(StompSession.Subscription::unsubscribe);
        this.newGameId.setOnMouseClicked(mouseEvent -> copyGameId());
        this.copyGameId.setOnMouseClicked(mouseEvent -> copyGameId());
        joinGame.setOnAction(actionEvent ->  joinExistingGame(this.gameId.getText()));
        createGame.setOnAction(actionEvent -> createGame());
        changeImage.setOnAction(actionEvent -> changeImage());
        saveImage.setOnAction(actionEvent -> saveNewImage());
        welcomeText.setText("Hallo "+ currentUser.getUsername()+"! Zeit für ein Du-Du-Duell!");
        loadUserData();
        loadAllAvatars();
        loadGameStatistics();
    }

    private void saveNewImage() {
        Avatar newAvatar = avatarList.get(avatarCount);
        EditUserDetailsTask editTask = new EditUserDetailsTask(currentUser, newAvatar);
        new Thread(editTask).start();
        editTask.setOnSucceeded(workerStateEvent -> {
            // TODO: Fix Task to update the image
        });
        editTask.setOnFailed(workerStateEvent -> {
            System.out.println("Neues Bild konnte nicht gespeichert werden");
        });
    }

    private void loadGameStatistics() {
        GetGameStatisticsTask getGameStatistics = new GetGameStatisticsTask(currentUser);
        new Thread(getGameStatistics).start();
        getGameStatistics.setOnSucceeded(workerStateEvent -> {
            for (GameStatistics gs: getGameStatistics.getValue()) {
                history.getItems().add(gs.toString());
            }
        });

        getGameStatistics.setOnFailed(workerStateEvent -> {
            System.out.println("Statistiken können nicht geladen werden");
        });
    }

    private void changeImage() {
        Image i = new Image("https://p-frei.de" + avatarList.get(avatarCount).getImagePath());
        this.profileImg.setImage(i);
        toggleSaveImageButton(avatarList.get(avatarCount));
        updateAvatarCount();

    }

    private void updateAvatarCount() {
        if(avatarCount < avatarList.size()-1) {
            avatarCount++;
        } else {
            avatarCount = 0;
        }
    }

    private boolean isSameImagePath(Avatar avatar) {
        return ("https://p-frei.de" + avatar.getImagePath()).equals(currentUser.getAvatar());
    }

    private void toggleSaveImageButton(Avatar avatar) {
        if (isSameImagePath(avatar)) {
            saveImage.setStyle("visibility:hidden;");
            saveImage.setDisable(true);
        } else {
            saveImage.setStyle("visibility:visible;");
            saveImage.setDisable(false);
        }
    }

    private void loadAllAvatars() {
        AvatarListTask avatarListTask = new AvatarListTask(currentUser);
        new Thread(avatarListTask).start();
        avatarListTask.setOnSucceeded(workerStateEvent -> {
            avatarList = avatarListTask.getValue();
        });
    }

    private void copyGameId() {
        StringSelection selection = new StringSelection(this.newGameId.getText());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
        this.copyGameId.setText("Kopiert!");
    }

    private void loadUserData() {
        if (currentUser != null) {
            UserDetailsTask userDetailsTask = new UserDetailsTask(currentUser);
            new Thread(userDetailsTask).start();
            userDetailsTask.setOnSucceeded(workerStateEvent -> {
                currentUser = userDetailsTask.getValue();
                holder.setUser(currentUser);
                profileImg.setImage(new Image(currentUser.getAvatar() ));
            });
            userDetailsTask.setOnFailed(workerStateEvent -> {
                System.out.println("load user data failed");
            });
        }
    }

    private void createGame() {
        PostCreateGameTaks task = new PostCreateGameTaks(currentUser.getJWT());
        new Thread(task).start();
        task.setOnSucceeded(workerStateEvent -> {
            String gameId = task.getValue();
            displayGameId(gameId);
        });
    }

    private void displayGameId(String gameId) {
        newGameIdContainer.setStyle("visibility: visible;");
        newGameId.setText(gameId);
        this.gameId.setText(gameId);
    }

    private void subscribeToGame(String gameId) {
        client.subscribe("/updates/" + gameId + "/user/" + currentUser.getUsername() );

        client.subscribe("/updates/" + gameId);

    }


    private void joinExistingGame(String gameId) {
        subscribeToGame(gameId);
        Action action = new AddUserToGameAction();
        client.sendMessage("/app/game/" + gameId, action);
        SkribbleGame game = new SkribbleGame();
        game.setGameId(gameId);
        SkribbleGameHolder.getInstance().setGame(game);
        ClientHolder.getInstance().setClient(client);
        UserHolder.getInstance().setUser(currentUser);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SceneChanger.change("before-start-view.fxml", anchor);
    }




}

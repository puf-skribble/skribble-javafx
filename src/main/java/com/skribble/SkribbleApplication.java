package com.skribble;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.stage.Popup;
import javafx.stage.Stage;

import java.io.IOException;

public class SkribbleApplication extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        Scene scene = FXMLLoader.load(getClass().getResource("login-view.fxml"));
        scene.getRoot().setStyle("-fx-font-family: 'Times New Roman'");
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch();
    }
}
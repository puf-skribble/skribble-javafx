package com.skribble.chat;

import javafx.beans.Observable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableObjectValue;

import java.util.ArrayList;

public class Chat  {
    private ObservableObjectValue<ArrayList<Chatmessage>> messages = new SimpleObjectProperty<ArrayList<Chatmessage>>();

    public void addMessage(Chatmessage message) {
        setMessages(message);
    }

    public void setMessages(Chatmessage message) {
        messages.get().add(message);
    }

    public ArrayList<Chatmessage> getMessages() {
        return messages.get();
    }

    public ObservableObjectValue<ArrayList<Chatmessage>> messagesProperty() {
        return messages;
    }
}

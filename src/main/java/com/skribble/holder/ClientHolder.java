package com.skribble.holder;

import com.skribble.api.websocket.SkribbleWebsocketClient;

/**
 *
 * Singelton, der eine Instanz des Websocket Clients hält
 * Dadurch kann von jedem Controller auf die selben Subsriptions zugegriffen werden und es ist sichergestellt,
 * dass es nur eine Connection zum Websocket gibt
 *
 */

 public final class ClientHolder {
    private SkribbleWebsocketClient c;
    private final static ClientHolder INSTANCE = new ClientHolder();

    public ClientHolder() {}

    public static ClientHolder getInstance() {
        return INSTANCE;
    }

    public void setClient(SkribbleWebsocketClient c) {
        this.c = c;
    }

    public SkribbleWebsocketClient getClient() {
        return this.c;
    }
}

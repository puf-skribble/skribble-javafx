package com.skribble.holder;

import com.skribble.SkribbleGame;

/**
 *
 * Singelton, der eine Instanz des Spiel Modells hält
 * So kann das Spiel von verschiedenen Controllern/Views aus manipuliert werden
 *
 */

public final class SkribbleGameHolder {
    private SkribbleGame game;
    private final static SkribbleGameHolder INSTANCE = new SkribbleGameHolder();

    private SkribbleGameHolder() {}

    public static SkribbleGameHolder getInstance() {
        return INSTANCE;
    }

    public void setGame(SkribbleGame u) {
        this.game = u;
    }

    public SkribbleGame getGame() {
        return this.game;
    }
}

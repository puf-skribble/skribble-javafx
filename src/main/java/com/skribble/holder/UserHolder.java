package com.skribble.holder;

import com.skribble.user.User;

/**
 * Singelton um den aktuellen User in jedem Controller nutzen zu können.
 * Ebenso gibt es stets nur ein Objekt für den Nutzer der Anwendung vorhanden
 */

public final class UserHolder {

    private User user;
    private final static UserHolder INSTANCE = new UserHolder();

    private UserHolder() {}

    public static UserHolder getInstance() {
        return INSTANCE;
    }

    public void setUser(User u) {
        this.user = u;
    }

    public User getUser() {
        return this.user;
    }
}

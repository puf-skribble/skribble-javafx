package com.skribble.drawingTools;

public enum ToolType {
    ERASER, PEN
}

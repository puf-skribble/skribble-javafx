package com.skribble.drawingTools;

import javafx.scene.paint.Color;


public class Tool {
    private Color color;
    private Pencilweight thickness;
    private ToolType type;

    public Tool() {
        this.type = ToolType.PEN;
        this.color = Color.BLACK;
        this.thickness = Pencilweight.KLEIN;
    }

    public ToolType getType() {
        return type;
    }

    public void setType(ToolType type) {
        this.type = type;
    }

    public Color getColor() {
        if (this.type == ToolType.ERASER) {
            System.out.println("white");
            return Color.WHITE;
        } else {
            return color;
        }
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setThickness(Pencilweight size) {
        this.thickness =size;
    }

    public Pencilweight getThickness() {
        return thickness;
    }

    public String colorToHex() {
        return "#" + colorChanelToHex(color.getRed())
                + colorChanelToHex(color.getGreen())
                + colorChanelToHex(color.getBlue());
    }

    private String colorChanelToHex(double chanelValue) {
        String rtn = Integer.toHexString((int) Math.min(Math.round(chanelValue * 255), 255));
        if (rtn.length() == 1) {
            rtn = "0" + rtn;
        }
        return rtn;
    }


    public double thicknessToDouble() {
        if (String.valueOf(this.thickness).equals("KLEIN")) {
            return 5;
        } else if (String.valueOf(this.thickness).equals("MITTEL")) {
            return 10;
        } else {
            return 20;
        }
    }

}

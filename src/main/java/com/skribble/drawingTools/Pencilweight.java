package com.skribble.drawingTools;

public enum Pencilweight {
    KLEIN, MITTEL, GROSS;
}

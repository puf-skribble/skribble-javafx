package com.skribble;


import com.skribble.api.tasks.PostLoginTask;
import com.skribble.api.tasks.PostRegisterTask;
import com.skribble.api.tasks.UserDetailsTask;
import com.skribble.holder.UserHolder;
import com.skribble.user.User;
import com.skribble.utils.SQLiteJDBC;
import com.skribble.utils.SceneChanger;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import org.joda.time.DateTimeComparator;

import java.sql.SQLException;

import org.joda.time.DateTime;


public class LoginController  {

    @FXML
    Button login;
    @FXML
    Button register;
    @FXML
    TextField username;
    @FXML
    TextField password;
    @FXML
    Text loginerror;
    @FXML
    Button changeViewtype;
    @FXML
    private AnchorPane anchorRoot;
    private SQLiteJDBC sql = SQLiteJDBC.getInstance();
    private User currentUser = null;
    private static final String REGISTER_ERROR_MSG = "Nutzername existiert bereits!";
    private static final String LOGIN_ERROR_MSG = "Username oder Passwort falsch!";


    @FXML
    protected void initialize() {
        jwtIsValid();
        login.setOnAction(actionEvent -> logUserIn());
        register.setOnAction(actionEvent -> registerUser());
        register.setDisable(true);
        changeViewtype.setOnAction(actionEvent -> changeLoginToRegister());
    }

    private void registerUser() {
        PostRegisterTask registerTask = new PostRegisterTask(username.getText(), password.getText());
        new Thread(registerTask).start();
        registerTask.setOnSucceeded((WorkerStateEvent e) -> {
            User user = registerTask.getValue();
            if (user != null) {
                logUserIn();
            }
            else {
                displayErrorMsg(REGISTER_ERROR_MSG);
            }
        });
    }



    private void logUserIn() {
        PostLoginTask loginTask = new PostLoginTask(username.getText(), password.getText());
        new Thread(loginTask).start();
        loginTask.setOnSucceeded((WorkerStateEvent e) -> {
            // TODO: eventuell Futures testen, soll wohl auf den Async Task warten
            User user = loginTask.getValue();
            if (user != null) {
                UserDetailsTask userDetailsTask = new UserDetailsTask(user);
                new Thread(userDetailsTask).start();
                userDetailsTask.setOnSucceeded(workerStateEvent -> {

                    User updatedUser = (User) userDetailsTask.getValue();
                    sql.saveUser(updatedUser);
                    currentUser = updatedUser;
                    loadNextScene();
                });

            }
            else {
                displayErrorMsg(LOGIN_ERROR_MSG);
            }
        });

        loginTask.setOnFailed((WorkerStateEvent e) -> {
            System.out.println("Login Task schlägt fehl");
        });

    }

    private void changeLoginToRegister() {
        if (login.getStyle() == "" || login.getStyle() == "visibility:visible;") {
            login.setStyle("visibility:hidden;");
            login.setDisable(true);
            register.setStyle("visibility:visible;");
            register.setDisable(false);
        } else {
            login.setStyle("visibility:visible;");
            login.setDisable(false);
            register.setStyle("visibility:hidden;");
            register.setDisable(true);
        }
    }

    private void displayErrorMsg(String msg) {
        this.loginerror.setText(msg);
        this.loginerror.setStyle("visibility:visible;");
    }

    @FXML
    private void loadNextScene() {
        UserHolder holder = UserHolder.getInstance();
        holder.setUser(currentUser);
        SceneChanger.change("home-view.fxml", anchorRoot);
    }


    private void jwtIsValid() {
        try {
            currentUser = sql.getUser() ;
            if (currentUser == null) {
                return;
            } else {
                String jwtExpire = currentUser.getJwtExpire();
                DateTime time =  DateTime.parse(jwtExpire);
                DateTime now = new DateTime();
                DateTimeComparator dateTimeComparator = DateTimeComparator.getInstance();
                int compareResult = dateTimeComparator.compare(time, now);
                if (compareResult == 1) {
                    UserHolder.getInstance().setUser(currentUser);
                    loadNextScene();
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }


    }


}

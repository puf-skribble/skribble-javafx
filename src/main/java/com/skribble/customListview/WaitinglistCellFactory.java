package com.skribble.customListview;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import com.skribble.user.User;

public class WaitinglistCellFactory implements Callback<ListView<User>, ListCell<User>> {

    @Override
    public ListCell<User> call(ListView<User> userListView) {
        return new ListCell<User>(){
            @Override
            public void updateItem(User user, boolean empty) {
                super.updateItem(user, empty);
                if (empty || user == null) {
                    setText(null);
                } else {
                    ImageView profileimg = new ImageView(new Image(user.getAvatar()));
                    profileimg.setFitWidth(100);
                    profileimg.setFitHeight(100);
                    setGraphic(profileimg);
                    setText(user.getUsername() + "\t\n ");
                }
            }
        };
    }

}

package com.skribble.customListview;

import com.skribble.api.websocket.actionHandling.response.RankingDTO;
import com.skribble.chat.Chatmessage;
import com.skribble.user.User;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Callback;
public class ChatCellFactory implements Callback<ListView<Chatmessage>, ListCell<Chatmessage>> {

    @Override
    public ListCell<Chatmessage> call(ListView<Chatmessage> chatListView) {
        return new ListCell<Chatmessage>(){
            @Override
            public void updateItem(Chatmessage chatmessage, boolean empty) {
                super.updateItem(chatmessage, empty);
                if (empty || chatmessage == null) {
                    setText(null);
                } else {
                    if (chatmessage.answerIsCorrect) {
                        Text txt = new Text();
                        txt.setText(chatmessage.senderId + "; " + chatmessage.answer);
                        txt.setFill(Color.GREEN);
                        setText(chatmessage.senderId + " hat das Wort erraten!");
                    } else {
                        setText(chatmessage.senderId + ": " + chatmessage.answer);
                    }
                }
            }
        };
    }

}
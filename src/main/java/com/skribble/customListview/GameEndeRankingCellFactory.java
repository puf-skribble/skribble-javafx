package com.skribble.customListview;

import com.skribble.api.websocket.actionHandling.response.RankingDTO;
import com.skribble.user.User;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

public class GameEndeRankingCellFactory implements Callback<ListView<RankingDTO>, ListCell<RankingDTO>> {

    @Override
    public ListCell<RankingDTO> call(ListView<RankingDTO> userListView) {
        return new ListCell<RankingDTO>(){
            @Override
            public void updateItem(RankingDTO ranking, boolean empty) {
                super.updateItem(ranking, empty);
                if (empty || ranking == null) {
                    setText(null);
                } else {
                    setText(ranking.rank + ". " + ranking.gameUserDTO.getUsername() + " - Score: " + ranking.score);
                    ImageView profileimg = new ImageView(new Image(ranking.gameUserDTO.getAvatar()));
                    profileimg.setFitWidth(60);
                    profileimg.setFitHeight(60);
                    setGraphic(profileimg);
                }
            }
        };
    }

}

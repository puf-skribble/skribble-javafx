package com.skribble.customListview;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import com.skribble.user.User;

public class LeaderboardCellFactory implements Callback<ListView<User>, ListCell<User>> {

    @Override
    public ListCell<User> call(ListView<User> userListView) {
        return new ListCell<User>(){
            @Override
            public void updateItem(User user, boolean empty) {
                super.updateItem(user, empty);
                if (empty || user == null) {
                    setText(null);
                } else {
                    setText(user.getUsername() + "\t\n " + user.getScore());
                    ImageView profileimg = new ImageView(new Image(user.getAvatar()));
                    profileimg.setFitWidth(60);
                    profileimg.setFitHeight(60);
                    setGraphic(profileimg);
                }
            }
        };
    }

}

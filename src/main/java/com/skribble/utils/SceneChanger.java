package com.skribble.utils;

import com.skribble.SkribbleController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class SceneChanger {

    public static void change(String newScene, AnchorPane anchorRoot) {
        try {
            AnchorPane root = FXMLLoader.load(SkribbleController.class.getResource(newScene));
            anchorRoot.getChildren().removeAll();
            anchorRoot.getChildren().add(root);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

package com.skribble.utils;

import com.skribble.user.User;

import java.sql.*;

public class SQLiteJDBC {

    private static SQLiteJDBC instance;
    private Connection conn = null;
    private Statement stmt;
    // Private constructor
    private SQLiteJDBC() {

        try {
            conn = DriverManager.getConnection("jdbc:sqlite:skribble.db");
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            boolean status = stmt.execute("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(50), jwt VARCHAR(50), jwtExpire VARCHAR(50))");
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }
    // Static access to singleton object
    public static synchronized SQLiteJDBC getInstance() {
        if (instance == null) { instance = new SQLiteJDBC(); }
        return instance;
    }

    public User getUser() throws SQLException {
        conn = DriverManager.getConnection("jdbc:sqlite:skribble.db");
        PreparedStatement prepStmt = conn.prepareStatement(
                "SELECT * FROM users ORDER BY id DESC LIMIT 1 ");
        ResultSet res = prepStmt.executeQuery();
        User u = null ;
        while ( res.next() ) {
            u = new User(res.getString("username"), res.getString("jwt"));
            u.setJwtExpire(res.getString("jwtExpire"));
        }
        prepStmt.close();
        return u;

    }

    public void saveUser(User user) {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:skribble.db");
            stmt = conn.createStatement();
            boolean status = stmt.execute("INSERT INTO users(username, jwt, jwtExpire) VALUES('" + user.getUsername() + "', '" + user.getJWT() + "', '" + user.getJwtExpire() + "')");
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropTable() {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:skribble.db");
            stmt = conn.createStatement();
            stmt.execute("DROP TABLE users");
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
package com.skribble.api.tasks;

import com.mashape.unirest.http.HttpResponse;
import com.skribble.api.BackendConnection;
import javafx.concurrent.Task;
import com.skribble.user.Avatar;
import com.skribble.user.User;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class UserDetailsTask extends Task<User> {
    private User user;
    private BackendConnection connection = BackendConnection.getInstance();

    public UserDetailsTask(User user) { this.user = user; }
    @Override
    protected User call() throws Exception {
        HttpResponse get = connection.get("backend/user/details", user.getJWT());
        String body = get.getBody().toString();
        JSONObject jsonBody = (JSONObject) JSONValue.parse(body);
        JSONObject jsonAvatar = (JSONObject) JSONValue.parse(jsonBody.get("avatar").toString());
        Avatar avatar = new Avatar();
        avatar.setName(jsonAvatar.get("name").toString());
        avatar.setImagePath(jsonAvatar.get("imagePath").toString());
        this.user.setAvatar(avatar);
        if (get.getStatus() == 200) {
            return this.user;
        } else {
            return null;
        }
    }
}

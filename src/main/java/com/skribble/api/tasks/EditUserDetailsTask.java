package com.skribble.api.tasks;

import com.mashape.unirest.http.HttpResponse;
import com.skribble.api.BackendConnection;
import javafx.concurrent.Task;
import com.skribble.user.Avatar;
import com.skribble.user.User;

public class EditUserDetailsTask extends Task<Boolean> {

    private BackendConnection connection = BackendConnection.getInstance();
    private User user;
    private Avatar avatar;
    public EditUserDetailsTask(User user, Avatar avatar) { this.user = user; this.avatar = avatar;}
    @Override
    protected Boolean call() throws Exception {

        HttpResponse post = connection.postAuth("backend/user/update", user.getJWT(), this.avatar );
        if (post.getStatus() == 201) {
            return true;
        } else {
            return false;
        }
    }
}
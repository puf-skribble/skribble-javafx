package com.skribble.api.tasks;

import com.mashape.unirest.http.HttpResponse;
import com.skribble.api.BackendConnection;
import javafx.concurrent.Task;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import com.skribble.user.User;


public class PostLoginTask extends Task<User> {
    private String username;
    private String password;
    private BackendConnection connection = BackendConnection.getInstance();


    public PostLoginTask(String username, String password) { this.username = username; this.password = password; }
    @Override
    protected User call() throws Exception {
        HttpResponse post = connection.post("backend/user/authenticate", username, password);
        if (post.getStatus() == 200) {
            String body = (String) post.getBody();
            JSONObject jsonBody = (JSONObject) JSONValue.parse(body);
            String jwt = jsonBody.get("jwt").toString();
            String jwtExpire = jsonBody.get("expiresAt").toString();
            User u = new User(username, jwt);
            u.setJwtExpire(jwtExpire);
            return u;
        } else {
            return null;
        }

    }
}

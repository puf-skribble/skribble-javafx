package com.skribble.api.tasks;

import com.mashape.unirest.http.HttpResponse;
import com.skribble.api.BackendConnection;
import javafx.concurrent.Task;

public class PostCreateGameTaks extends Task<String> {
    private String jwt;
    private String password;
    private BackendConnection connection = BackendConnection.getInstance();

    public PostCreateGameTaks(String jwt) { this.jwt = jwt; }
    @Override
    protected String call() throws Exception {
        HttpResponse post = connection.postAuth("backend/game", jwt);
        if (post.getStatus() == 200) {
            return post.getBody().toString();
        } else {
            return null;
        }
    }
}

package com.skribble.api.tasks;

import com.mashape.unirest.http.HttpResponse;
import com.skribble.api.BackendConnection;
import javafx.concurrent.Task;

public class GetCheckAuthTask extends Task<Boolean> {
    private String jwt;
    private BackendConnection connection = BackendConnection.getInstance();

    public GetCheckAuthTask(String jwt) { this.jwt = jwt;}
    @Override
    protected Boolean call() throws Exception {
        HttpResponse get = connection.get("checkAuth", jwt);
        return get.getStatus() != 403;
    }
}

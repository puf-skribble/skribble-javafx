package com.skribble.api.tasks;

import com.mashape.unirest.http.HttpResponse;
import com.skribble.api.BackendConnection;
import javafx.concurrent.Task;
import com.skribble.user.User;

public class PostRegisterTask extends Task<User> {
    private String username;
    private String password;
    private BackendConnection connection = BackendConnection.getInstance();

    public PostRegisterTask(String username, String password) { this.username = username; this.password = password; }
    @Override
    protected User call() throws Exception {
        HttpResponse post = connection.post("backend/user/signup", username, password);
        if (post.getStatus() == 201) {
            return new User(username, null);
        } else {
            return null;
        }
    }
}
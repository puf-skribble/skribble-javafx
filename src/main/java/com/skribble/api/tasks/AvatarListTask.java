package com.skribble.api.tasks;

import com.mashape.unirest.http.HttpResponse;
import com.skribble.api.BackendConnection;
import javafx.concurrent.Task;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import com.skribble.user.Avatar;
import com.skribble.user.User;

import java.util.ArrayList;

public class AvatarListTask extends Task<ArrayList<Avatar>> {
    private BackendConnection connection = BackendConnection.getInstance();
    private User user;
    public AvatarListTask(User user) { this.user = user; }
    @Override
    protected ArrayList<Avatar> call() throws Exception {
        HttpResponse get = connection.get("avatar", user.getJWT());
        String body = get.getBody().toString();
        JSONObject jsonBody = (JSONObject) JSONValue.parse(body);
        ArrayList<Avatar> avatars = new ArrayList<>();

        for (Object avatarName : jsonBody.keySet()) {
            Avatar a = new Avatar();
            a.setName(avatarName.toString());
            a.setImagePath(jsonBody.get(avatarName.toString()).toString());
            avatars.add(a);
        }

        if (get.getStatus() == 200) {
            return avatars;
        } else {
            return null;
        }
    }
}

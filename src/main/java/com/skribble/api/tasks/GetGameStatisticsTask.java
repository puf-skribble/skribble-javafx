package com.skribble.api.tasks;

import com.mashape.unirest.http.HttpResponse;
import com.skribble.api.BackendConnection;
import javafx.concurrent.Task;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import com.skribble.user.GameStatistics;
import com.skribble.user.User;

import java.util.ArrayList;

public class GetGameStatisticsTask extends Task<ArrayList<GameStatistics>> {
    private User user;
    private BackendConnection connection = BackendConnection.getInstance();

    public GetGameStatisticsTask(User user) { this.user = user;}
    @Override
    protected  ArrayList<GameStatistics> call() throws Exception {
        HttpResponse get = connection.get("gameStatistic", user.getJWT());
        ArrayList<GameStatistics> stats = new ArrayList<>();
        if (get.getStatus() == 200) {
            JSONArray body = (JSONArray) JSONValue.parse(get.getBody().toString());
            for (Object o : body) {
                JSONObject parsedBody = (JSONObject) o;
                GameStatistics gs = new GameStatistics();
                gs.setPlayersCount((long)parsedBody.get("playersCount"));
                gs.setRank((long) parsedBody.get("rank"));
                gs.setScore((long) parsedBody.get("score"));
                gs.setPlayerGameNumber((long) parsedBody.get("playerGameNumber"));
                gs.setTimePlayed(parsedBody.get("timePlayed").toString());
                stats.add(gs);
            }


        } else {

            return null;
        }
        return stats;
    }
}

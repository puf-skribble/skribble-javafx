package com.skribble.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.skribble.user.Avatar;
import com.skribble.user.UserUpdateDTO;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import java.io.IOException;

public class BackendConnection {
    // Singleton object
    private static BackendConnection instance;
    private String base = "https://p-frei.de/";
    // Private constructor
    private BackendConnection() {}
    // Static access to singleton object
    public static synchronized BackendConnection getInstance() {
        if (instance == null) { instance = new BackendConnection(); }
        return instance;
    }

    public HttpResponse post(String host, String username, String password) throws IOException {
        String url = host;
        String json = "{\"userName\": \"" + username + "\", \"password\": \"" + password + "\"}";

        try {
            HttpResponse<String> res = Unirest.post(base + url).header("Content-Type", "application/json").body(json).asString();
            return res;
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HttpResponse postAuth(String host, String jwt) {
        String url = host;

        try {
            HttpResponse<String> res = Unirest.post(base + url).header("Content-Type", "application/json").header("Authorization", "Bearer " + jwt).asString();
            return res;

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HttpResponse postAuth(String host, String jwt, Object body) {
        String url = host;
        Avatar a = (Avatar) body;
        UserUpdateDTO userUpdate = new UserUpdateDTO();
        userUpdate.setAvatar(a.getName());
        System.out.println(a.getName());
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(userUpdate);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            HttpResponse<String> res = Unirest.post(base + url).header("Content-Type", "application/json").header("Authorization", "Bearer " + jwt).body(jsonString).asString();
            System.out.println(res.getStatus());
            return res;

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HttpResponse get(String host, String jwt) throws IOException {
        String url = host;

        try {
            HttpResponse<String> res = Unirest.get(base + url).header("Content-Type", "application/json").header("Authorization", "Bearer " + jwt).asString();
            return res;

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

}

package com.skribble.api.websocket;

import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.util.ArrayList;

public class SkribbleWebsocketClient {

    private WebSocketStompClient stompClient;
    public ArrayList<StompSession.Subscription> subscriptions = new ArrayList<>();
    private  StompSessionHandler sessionHandler;
    private String gameId;

    public SkribbleWebsocketClient(String url, String jwt) {
        connect(url, jwt);
    }

    public void connect(String url, String jwt) {
        String URL = url;
        WebSocketClient client = new StandardWebSocketClient();
        stompClient = new WebSocketStompClient(client);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        sessionHandler = new SkribbleStompSessionHandler();
        StompHeaders headers = new StompHeaders();
        WebSocketHttpHeaders httpHeaders = new WebSocketHttpHeaders();
        headers.setLogin("Bearer " + jwt );
        stompClient.connect(URL, httpHeaders, headers, sessionHandler);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void subscribe(String path) {
        subscriptions.add(((SkribbleStompSessionHandler) sessionHandler).subscribeToChannel(path));
    }

    public void sendMessage(String path, Object action) {
        ((SkribbleStompSessionHandler) sessionHandler).sendMessage(path, action);
    }

    public void unsubscribe() {
        for (StompSession.Subscription subscription: subscriptions) {
            subscription.unsubscribe();
        }
    }


}

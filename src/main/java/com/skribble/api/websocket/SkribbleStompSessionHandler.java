package com.skribble.api.websocket;

import com.skribble.SkribbleController;
import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.factory.ActionsResponseFactory;
import com.skribble.api.websocket.actionHandling.response.strategies.ActionResponseStrategy;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;

import java.lang.reflect.Type;

public class SkribbleStompSessionHandler implements StompSessionHandler {
    private StompSession session = null;
    private SkribbleController observer = null;

    @Override
    public void afterConnected(StompSession stompSession, StompHeaders stompHeaders) {
        System.out.println("Connection hergestellt");
        this.session = stompSession;

    }

    @Override
    public void handleException(StompSession stompSession, StompCommand stompCommand, StompHeaders stompHeaders, byte[] bytes, Throwable throwable) {
        System.out.println(throwable);
        throwable.printStackTrace();
    }

    @Override
    public void handleTransportError(StompSession stompSession, Throwable throwable) {
        System.out.println(throwable);

        throwable.printStackTrace();
    }

    @Override
    public Type getPayloadType(StompHeaders stompHeaders) {
        return ActionResponse.class;
    }

    @Override
    public void handleFrame(StompHeaders stompHeaders, Object o) {
        ActionResponse msg = (ActionResponse) o;

        ActionsResponseFactory factory = new ActionsResponseFactory();
        ActionResponseStrategy strategy = factory.getStrategyByAction(msg.getActionResponseType());
        strategy.run(msg);
    }

    public StompSession.Subscription subscribeToChannel(String path) {
        return this.session.subscribe(path, this);
    }



    public void sendMessage(String path, Object message) {
        this.session.send(path, message);
    }


}

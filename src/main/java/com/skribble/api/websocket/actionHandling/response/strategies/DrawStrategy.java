package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.api.websocket.actionHandling.response.Draw;
import com.skribble.api.websocket.actionHandling.response.DrawerWord;
import com.skribble.holder.SkribbleGameHolder;

public class DrawStrategy extends ActionResponseStrategy {
    public DrawStrategy( ) {
        super(ActionResponseType.draw);
    }

    @Override
    public void run(ActionResponse actionResponse) {
        Draw draw = (Draw) actionResponse;
        SkribbleGameHolder.getInstance().getGame().drawOpponentInput(draw.getPixelToUpdate());
    }
}

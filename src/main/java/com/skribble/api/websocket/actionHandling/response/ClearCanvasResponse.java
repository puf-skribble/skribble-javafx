package com.skribble.api.websocket.actionHandling.response;

public class ClearCanvasResponse extends ActionResponse{
    private static final ActionResponseType ACTION_RESPONSE_TYPE = ActionResponseType.clearCanvas;

    public ClearCanvasResponse() {
        super(ACTION_RESPONSE_TYPE);
    }
    @Override
    public ActionResponseType getActionResponseType() {
        return ACTION_RESPONSE_TYPE;
    }
}

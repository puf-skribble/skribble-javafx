package com.skribble.api.websocket.actionHandling.inputs;

public class ChatAction extends Action {
    private String message;
    private  static final ActionType actionType = ActionType.chat;

    public ChatAction(String message) {
        super(actionType);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ActionType  getActionType() {
        return this.actionType;
    }
}

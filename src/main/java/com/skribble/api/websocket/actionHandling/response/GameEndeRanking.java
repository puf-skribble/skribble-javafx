package com.skribble.api.websocket.actionHandling.response;


import java.util.List;

public class GameEndeRanking extends ActionResponse{
    private static final ActionResponseType ACTION_RESPONSE_TYPE = ActionResponseType.gameEndedRanking;
    private List<RankingDTO> resultRanking;

    public GameEndeRanking() {
        super(ACTION_RESPONSE_TYPE);
    }

    @Override
    public ActionResponseType getActionResponseType() {
        return ACTION_RESPONSE_TYPE;
    }

    public List<RankingDTO> getResultRanking() {
        return resultRanking;
    }

    public void setResultRanking(List<RankingDTO> resultRanking) {
        this.resultRanking = resultRanking;
    }
}

package com.skribble.api.websocket.actionHandling.inputs;

public enum ActionType {
        chat,draw,addUserToGame,startGame,clearCanvas;
}

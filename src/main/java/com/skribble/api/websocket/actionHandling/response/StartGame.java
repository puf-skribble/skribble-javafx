package com.skribble.api.websocket.actionHandling.response;

public class StartGame extends ActionResponse{
    private static final ActionResponseType ACTION_RESPONSE_TYPE = ActionResponseType.startGame;

    public StartGame() {
        super(ACTION_RESPONSE_TYPE);
    }
    @Override
    public ActionResponseType getActionResponseType() {
        return ACTION_RESPONSE_TYPE;
    }
}

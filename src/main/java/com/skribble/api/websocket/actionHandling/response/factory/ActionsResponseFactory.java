package com.skribble.api.websocket.actionHandling.response.factory;

import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.api.websocket.actionHandling.response.GameEndeRanking;
import com.skribble.api.websocket.actionHandling.response.strategies.*;

import java.util.ArrayList;

/**
 * In der Factory werden alle verfügbaren Strategien in einer ArrayListe gespeichert.
 * Über den AktionResponseType kann die passende Strategie zur eingehenden Websocket Aktion ausgewählt werden.
 */

public class ActionsResponseFactory {
    private ArrayList<ActionResponseStrategy> actionList = new ArrayList<>();

    public ActionsResponseFactory() {

        actionList.add(new DrawerWordStrategy());
        actionList.add(new GameEndedRankingStrategty());
        actionList.add(new NewRoundStrategy());
        actionList.add(new RoundStatisticsStrategy());
        actionList.add(new StartGameStrategy());
        actionList.add(new UserJoinedStrategy());
        actionList.add(new DrawStrategy());
        actionList.add(new ChatStrategy());
        actionList.add(new ClearCanvasStrategy());
    }

    public ActionResponseStrategy getStrategyByAction(ActionResponseType actionResponseType) {

        final ActionResponseStrategy[] returnValue = new ActionResponseStrategy[1];
        actionList.forEach(actionResponseStrategy -> {
            if (actionResponseStrategy.getActionResponseType().toString().equals(actionResponseType.toString())) {
                returnValue[0] = actionResponseStrategy;
            }
        });
        return returnValue[0];
    }
}

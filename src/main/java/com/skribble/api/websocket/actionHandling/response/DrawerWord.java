package com.skribble.api.websocket.actionHandling.response;

public class DrawerWord extends ActionResponse {
    private static final ActionResponseType ACTION_RESPONSE_TYPE = ActionResponseType.drawerWord;
    private WordDTO wordToDraw;

    public DrawerWord() {
        super(ACTION_RESPONSE_TYPE);
    }

    @Override
    public ActionResponseType getActionResponseType() {
        return ACTION_RESPONSE_TYPE;
    }

    public WordDTO getWordToDraw() {
        return wordToDraw;
    }

    public void setWordToDraw(WordDTO wordToDraw) {
        this.wordToDraw = wordToDraw;
    }

}

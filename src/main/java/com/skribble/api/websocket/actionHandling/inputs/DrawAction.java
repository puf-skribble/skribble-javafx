package com.skribble.api.websocket.actionHandling.inputs;

import com.skribble.api.websocket.actionHandling.response.RankingDTO;

public class DrawAction extends Action{
    private  static final ActionType actionType = ActionType.draw;
    private RankingDTO.PixelDTO pixelToUpdate;
    public DrawAction() {
        super(actionType);
    }

    public ActionType getActionType() {
        return this.actionType;
    }

    public RankingDTO.PixelDTO getPixelToUpdate() {
        return pixelToUpdate;
    }

    public void setPixelToUpdate(RankingDTO.PixelDTO pixelToUpdate) {
        this.pixelToUpdate = pixelToUpdate;
    }
}

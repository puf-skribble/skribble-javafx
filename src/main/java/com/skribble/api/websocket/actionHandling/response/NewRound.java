package com.skribble.api.websocket.actionHandling.response;

import com.skribble.user.User;

public class NewRound extends ActionResponse{
    private static final ActionResponseType ACTION_RESPONSE_TYPE = ActionResponseType.newRound;
    private User drawer;
    private int lengthOfWord;
    private int roundNumber;

    public NewRound() {
        super(ACTION_RESPONSE_TYPE);
    }

    @Override
    public ActionResponseType getActionResponseType() {
        return ACTION_RESPONSE_TYPE;
    }

    public User getDrawer() {
        return drawer;
    }

    public void setDrawer(User drawer) {
        this.drawer = drawer;
    }

    public int getLengthOfWord() {
        return lengthOfWord;
    }

    public void setLengthOfWord(int lengthOfWord) {
        this.lengthOfWord = lengthOfWord;
    }

    public int getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }
}

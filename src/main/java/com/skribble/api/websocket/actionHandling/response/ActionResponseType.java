package com.skribble.api.websocket.actionHandling.response;

public enum ActionResponseType {
    userJoined, chat, startGame, roundStatistics, newRound, drawerWord, clearCanvas, gameEndedRanking, draw;
}

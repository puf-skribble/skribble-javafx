package com.skribble.api.websocket.actionHandling.inputs;

public class ClearCanvasAction extends Action{
    private  static final ActionType actionType = ActionType.clearCanvas;

    public ClearCanvasAction() {
        super(actionType);
    }

    public ActionType getActionType() {
        return this.actionType;
    }
}

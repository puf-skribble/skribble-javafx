package com.skribble.api.websocket.actionHandling.response;

public class Draw extends ActionResponse {
    private static final ActionResponseType ACTION_RESPONSE_TYPE = ActionResponseType.draw;
    private RankingDTO.PixelDTO pixelToUpdate;
    public Draw() {
        super(ACTION_RESPONSE_TYPE);
    }

    public RankingDTO.PixelDTO getPixelToUpdate() {
        return pixelToUpdate;
    }

    public void setPixelToUpdate(RankingDTO.PixelDTO pixelToUpdate) {
        this.pixelToUpdate = pixelToUpdate;
    }

    @Override
    public ActionResponseType getActionResponseType() {
        return ACTION_RESPONSE_TYPE;
    }





}

package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.SkribbleGame;
import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.api.websocket.actionHandling.response.GameEndeRanking;
import com.skribble.api.websocket.actionHandling.response.RankingDTO;
import com.skribble.holder.SkribbleGameHolder;

public class GameEndedRankingStrategty extends  ActionResponseStrategy {
    public GameEndedRankingStrategty() {
        super(ActionResponseType.gameEndedRanking);
    }

    @Override
    public void run(ActionResponse actionResponse) {
        GameEndeRanking gameEndeRanking = (GameEndeRanking) actionResponse;
        SkribbleGameHolder.getInstance().getGame().gameEndedProperty().set(true);
        for (RankingDTO r : gameEndeRanking.getResultRanking()) {
            System.out.println(r.gameUserDTO.getUsername());
        }
        SkribbleGameHolder.getInstance().getGame().setRankingDTOS(gameEndeRanking.getResultRanking());
    }
}

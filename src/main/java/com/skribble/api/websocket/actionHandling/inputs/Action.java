package com.skribble.api.websocket.actionHandling.inputs;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "actionType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AddUserToGameAction.class, name = "addUserToGame"),
        @JsonSubTypes.Type(value = DrawAction.class, name = "draw"),
        @JsonSubTypes.Type(value = ChatAction.class, name = "chat"),
        @JsonSubTypes.Type(value = StartGameAction.class, name = "startGame")
})
/**
 * Eine Aktion wird an den Websocket Endpunkt gesendet. Dadurch kann z.B. ein User zum Spiel hinzugefügt werden
 */
public abstract class Action {
     private ActionType actionType;
     protected Action(ActionType actionType) {
          this.actionType = actionType;
     }

     public abstract ActionType getActionType();

}

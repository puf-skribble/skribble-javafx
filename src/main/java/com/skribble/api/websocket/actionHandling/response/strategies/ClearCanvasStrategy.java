package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.holder.SkribbleGameHolder;

public class ClearCanvasStrategy extends ActionResponseStrategy {
    public ClearCanvasStrategy( ) {
        super(ActionResponseType.clearCanvas);
    }

    @Override
    public void run(ActionResponse actionResponse) {
        SkribbleGameHolder.getInstance().getGame().clearCanvas();
    }
}

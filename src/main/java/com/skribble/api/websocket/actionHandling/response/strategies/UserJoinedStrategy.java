package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.SkribbleGame;
import com.skribble.holder.SkribbleGameHolder;
import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.api.websocket.actionHandling.response.UserJoined;
import com.skribble.user.User;

public class UserJoinedStrategy extends ActionResponseStrategy {
    public UserJoinedStrategy( ) {
        super(ActionResponseType.userJoined);
    }

    @Override
    public void run(ActionResponse actionResponse) {
        SkribbleGame game = SkribbleGameHolder.getInstance().getGame();
        UserJoined userJoined = (UserJoined) actionResponse;
        game.setGameHasStarted(userJoined.isHasGameStarted());
        game.setLeaderboard(userJoined.getAllUser());

    }
}

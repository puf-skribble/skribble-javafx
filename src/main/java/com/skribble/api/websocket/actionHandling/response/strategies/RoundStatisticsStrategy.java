package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.api.websocket.actionHandling.response.RoundStatistics;
import com.skribble.holder.SkribbleGameHolder;

public class RoundStatisticsStrategy extends ActionResponseStrategy {
    public RoundStatisticsStrategy( ) {
        super(ActionResponseType.roundStatistics);
    }

    @Override
    public void run(ActionResponse actionResponse) {
        System.out.println("Roundstatistics");
        SkribbleGameHolder.getInstance().getGame().setGameHasStarted(false);
        SkribbleGameHolder.getInstance().getGame().updatePoints(((RoundStatistics) actionResponse).getRoundPoints());
        SkribbleGameHolder.getInstance().getGame().revealWord(((RoundStatistics) actionResponse).getCorrectWord());


    }
}

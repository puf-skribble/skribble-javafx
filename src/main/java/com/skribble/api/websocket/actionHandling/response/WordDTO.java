package com.skribble.api.websocket.actionHandling.response;

public class WordDTO {
    public String id;
    public String category;
    public String difficulty;
    public String value;
    public int wordLenth;
}

package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.SkribbleGame;
import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.holder.SkribbleGameHolder;
import com.skribble.utils.SceneChanger;

public class StartGameStrategy extends ActionResponseStrategy {
    public StartGameStrategy( ) {
        super(ActionResponseType.startGame);
    }

    @Override
    public void run(ActionResponse actionResponse) {
        SkribbleGameHolder.getInstance().getGame().setOpenGameSceneProp(true);
    }
}

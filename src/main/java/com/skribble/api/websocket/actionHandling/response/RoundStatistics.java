package com.skribble.api.websocket.actionHandling.response;


import java.util.Map;

public class RoundStatistics extends ActionResponse{
    private static final ActionResponseType ACTION_RESPONSE_TYPE = ActionResponseType.roundStatistics;

    private int roundNumber;
    private String correctWord;
    private Map<String, Integer> roundPoints;

    public RoundStatistics() {
        super(ACTION_RESPONSE_TYPE);
    }
    @Override
    public ActionResponseType getActionResponseType() {
        return ACTION_RESPONSE_TYPE;
    }

    public int getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }

    public String getCorrectWord() {
        return correctWord;
    }

    public void setCorrectWord(String correctWord) {
        this.correctWord = correctWord;
    }

    public Map<String, Integer> getRoundPoints() {
        return roundPoints;
    }

    public void setRoundPoints(Map<String, Integer> roundPoints) {
        this.roundPoints = roundPoints;
    }
}

package com.skribble.api.websocket.actionHandling.inputs;

public class StartGameAction extends Action {
    private int rounds;
    private  static final ActionType actionType = ActionType.startGame;

    public StartGameAction(int rounds) {
        super(actionType);
        this.rounds = rounds;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    @Override
    public ActionType getActionType() {
        return ActionType.startGame;
    }
}

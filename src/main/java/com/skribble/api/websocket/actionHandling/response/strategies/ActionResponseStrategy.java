package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;

/**
 * Die Strategien können dann das Spiel im SkribbleGameHolder updaten
 */

public abstract class ActionResponseStrategy {
    private ActionResponseType actionResponseType;

    public ActionResponseStrategy(ActionResponseType actionResponseType) {
        this.actionResponseType = actionResponseType;
    }

    public ActionResponseType getActionResponseType() {return this.actionResponseType;};

    public abstract void run(ActionResponse actionResponse);
}

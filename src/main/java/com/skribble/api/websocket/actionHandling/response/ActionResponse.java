package com.skribble.api.websocket.actionHandling.response;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "actionResponseType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = UserJoined.class, name = "userJoined"),
        @JsonSubTypes.Type(value = DrawerWord.class, name = "drawerWord"),
        @JsonSubTypes.Type(value = RoundStatistics.class, name = "roundStatistics"),
        @JsonSubTypes.Type(value = NewRound.class, name = "newRound"),
        @JsonSubTypes.Type(value = StartGame.class, name = "startGame"),
        @JsonSubTypes.Type(value = GameEndeRanking.class, name = "gameEndedRanking"),
        @JsonSubTypes.Type(value = Draw.class, name = "draw"),
        @JsonSubTypes.Type(value = ChatActionResponse.class, name = "chat"),
        @JsonSubTypes.Type(value = ClearCanvasResponse.class, name = "clearCanvas"),
})

/**
 * Informationen aus der Websocket Aktion werden in diesem Objekt gespeichert.
 * So können z.B.: eingehende Chat-Nachrichten aufgenommen und verarbeitet werden
 */

public abstract class ActionResponse {

    private ActionResponseType actionResponseType;

    protected ActionResponse(ActionResponseType actionResponseType) {
        this.actionResponseType = actionResponseType;
    }

    public abstract ActionResponseType getActionResponseType();
}

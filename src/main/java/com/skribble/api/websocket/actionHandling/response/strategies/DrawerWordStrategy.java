package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.api.websocket.actionHandling.response.DrawerWord;
import com.skribble.api.websocket.actionHandling.response.UserJoined;
import com.skribble.holder.SkribbleGameHolder;

public class DrawerWordStrategy extends ActionResponseStrategy {
    public DrawerWordStrategy( ) {
        super(ActionResponseType.drawerWord);
    }

    @Override
    public void run(ActionResponse actionResponse) {
        DrawerWord drawerWord = (DrawerWord) actionResponse;
        SkribbleGameHolder.getInstance().getGame().setDrawerWord(drawerWord);
    }
}

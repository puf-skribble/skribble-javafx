package com.skribble.api.websocket.actionHandling.response;


import com.skribble.drawingTools.Pencilweight;
import com.skribble.user.User;

public class RankingDTO {
    public int rank;
    public User gameUserDTO;
    public int score;

    public static class PixelDTO {
        Pencilweight pencilweight;
        String drawcolor;
        Integer yStartPosition;
        Integer xStartPosition;
        Integer yEndPosition;
        Integer xEndPosition;

        public PixelDTO() {
        }

        public Pencilweight getPencilweight() {
            return pencilweight;
        }
    /*
        public void setPencilweight(Pencilweight pencilweight) {
            this.pencilweight = pencilweight;
        } */
        public void setPencilweight(Pencilweight pencilweight) {
            this.pencilweight = pencilweight;
        }

        public Integer getyEndPosition() {
            return yEndPosition;
        }

        public void setyEndPosition(Integer yEndPosition) {
            this.yEndPosition = yEndPosition;
        }

        public Integer getxEndPosition() {
            return xEndPosition;
        }

        public void setxEndPosition(Integer xEndPosition) {
            this.xEndPosition = xEndPosition;
        }


        public String getDrawcolor() {
            return drawcolor;
        }

        public void setDrawcolor(String drawcolor) {
            this.drawcolor = drawcolor;
        }

        public Integer getyStartPosition() {
            return yStartPosition;
        }

        public void setyStartPosition(Integer yStartPosition) {
            this.yStartPosition = yStartPosition;
        }

        public Integer getxStartPosition() {
            return xStartPosition;
        }

        public void setxStartPosition(Integer xStartPosition) {
            this.xStartPosition = xStartPosition;
        }
        public double thicknessToDouble() {
            if (String.valueOf(this.pencilweight).equals("KLEIN")) {
                return 10;
            } else if (String.valueOf(this.pencilweight).equals("MITTEL")) {
                return 20;
            } else {
                return 40;
            }
        }
    }
}

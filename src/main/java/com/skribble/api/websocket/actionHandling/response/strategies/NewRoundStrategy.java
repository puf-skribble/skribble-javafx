package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.api.websocket.actionHandling.response.NewRound;
import com.skribble.holder.SkribbleGameHolder;

public class NewRoundStrategy extends ActionResponseStrategy {
    public NewRoundStrategy( ) {
        super(ActionResponseType.newRound);
    }

    @Override
    public void run(ActionResponse actionResponse) {
        System.out.println("new Round");
        NewRound newRound = (NewRound) actionResponse;
        SkribbleGameHolder.getInstance().getGame().setDrawer(newRound.getDrawer());
        SkribbleGameHolder.getInstance().getGame().setRoundNumber(newRound.getRoundNumber());
        SkribbleGameHolder.getInstance().getGame().setLengthOfWord(newRound.getLengthOfWord());
        SkribbleGameHolder.getInstance().getGame().setGameHasStarted(true);
    }
}

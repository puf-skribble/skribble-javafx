package com.skribble.api.websocket.actionHandling.inputs;

public class AddUserToGameAction extends Action{
    private  static final ActionType actionType = ActionType.addUserToGame;

    public AddUserToGameAction() {
        super(actionType);
    }

    public ActionType getActionType() {
        return this.actionType;
    }
}

package com.skribble.api.websocket.actionHandling.response.strategies;

import com.skribble.api.websocket.actionHandling.response.ActionResponse;
import com.skribble.api.websocket.actionHandling.response.ActionResponseType;
import com.skribble.api.websocket.actionHandling.response.ChatActionResponse;
import com.skribble.api.websocket.actionHandling.response.Draw;
import com.skribble.holder.SkribbleGameHolder;

public class ChatStrategy extends ActionResponseStrategy {
    public ChatStrategy( ) {
        super(ActionResponseType.chat);
    }

    @Override
    public void run(ActionResponse actionResponse) {
        ChatActionResponse response = (ChatActionResponse) actionResponse;
        SkribbleGameHolder.getInstance().getGame().userGuessedWord(response);

    }
}

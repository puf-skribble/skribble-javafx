package com.skribble;

import com.skribble.api.websocket.actionHandling.inputs.Action;
import com.skribble.api.websocket.actionHandling.inputs.DrawAction;
import com.skribble.api.websocket.actionHandling.response.ChatActionResponse;
import com.skribble.api.websocket.actionHandling.response.DrawerWord;
import com.skribble.api.websocket.actionHandling.response.RankingDTO;
import com.skribble.chat.Chatmessage;
import com.skribble.drawingTools.Tool;
import com.skribble.holder.ClientHolder;
import com.skribble.holder.UserHolder;
import com.skribble.user.User;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class SkribbleGame {
    private final User currentUser;
    private Tool tool;
    private boolean isReleased = false;
    private GraphicsContext gc;
    private ObservableList<User> leaderboard;
    private String gameId;
    private RankingDTO.PixelDTO oldPixel = new RankingDTO.PixelDTO();
    private User drawer;
    private StringProperty drawerWord = new SimpleStringProperty();
    private BooleanProperty hasGameStarted = new SimpleBooleanProperty();
    private BooleanProperty openGameSceneProp = new SimpleBooleanProperty();
    private BooleanProperty gameEnded = new SimpleBooleanProperty();
    private IntegerProperty roundNumber = new SimpleIntegerProperty();
    private IntegerProperty lengthOfWord = new SimpleIntegerProperty();
    private StringProperty userGuessedCorrect = new SimpleStringProperty();
    private ObservableList<RankingDTO> rankingDTOS;
    private ObservableList<Chatmessage> chat;
    public SkribbleGame() {
        currentUser = UserHolder.getInstance().getUser();
        openGameSceneProp.set(false);
        gameEnded.set(false);
        hasGameStarted.set(false);
        this.tool = new Tool();
        chat = FXCollections.observableList(new ArrayList<Chatmessage>());
        leaderboard = FXCollections.observableArrayList();
        rankingDTOS = FXCollections.observableArrayList();
    }

    public void setGc(GraphicsContext gc) {
        this.gc = gc;
    }

    public void setLeaderboard(List<User> userList) {
       leaderboard.removeAll(leaderboard);
        for (User user: userList) {
          leaderboard.add(user);
        }
    }

    public ObservableList<User> getLeaderboard() {
        return leaderboard;
    }

    public Tool getTool() {
        return this.tool;
    }

    public String getGameId() {
        return this.gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public BooleanProperty isHasGameStarted() {
        return hasGameStarted;
    }

    public void setGameHasStarted(boolean hasGameStarted) {
        this.hasGameStarted.set(hasGameStarted);
    }

    public void draw(MouseEvent event) {
        if (UserHolder.getInstance().getUser().getUsername().equals(drawer.getUsername())) {
            if (isReleased) {
                gc.moveTo(event.getX(), event.getY());
                isReleased = false;
            }
            gc.setStroke(tool.getColor());
            gc.setLineCap(StrokeLineCap.ROUND);
            gc.setLineWidth(tool.thicknessToDouble());
            gc.lineTo(event.getX(), event.getY());
            gc.stroke();
            gc.closePath();
            gc.beginPath();
            gc.moveTo(event.getX(), event.getY());
            RankingDTO.PixelDTO currentPixel = new RankingDTO.PixelDTO();
            if (oldPixel != null) {
                if (event.getX() >= 0 && event.getY() >= 0 && event.getX() <= 900 && event.getY() <= 678) {
                    currentPixel.setDrawcolor(tool.colorToHex());
                    currentPixel.setPencilweight(tool.getThickness());
                    currentPixel.setxEndPosition((int) event.getX());
                    currentPixel.setyEndPosition((int) event.getY());
                    currentPixel.setxStartPosition(oldPixel.getxEndPosition());
                    currentPixel.setyStartPosition( oldPixel.getyEndPosition());
                    Action drawAction = new DrawAction();
                    ((DrawAction) drawAction).setPixelToUpdate(currentPixel);
                    ClientHolder.getInstance().getClient().sendMessage("/app/game/" + gameId, drawAction);
                    oldPixel = currentPixel;
                }
            } else {
                oldPixel.setxEndPosition((int) event.getX());
                oldPixel.setyEndPosition((int) event.getY());
            }
        }
    }

    public void stopDraw(MouseEvent event) {
        isReleased = true;
        if (oldPixel != null) {
            RankingDTO.PixelDTO currentPixel = new RankingDTO.PixelDTO();
            currentPixel.setDrawcolor(tool.colorToHex());
            currentPixel.setPencilweight(tool.getThickness());
            currentPixel.setxStartPosition(oldPixel.getxEndPosition());
            currentPixel.setyStartPosition( oldPixel.getyEndPosition());
            ClientHolder.getInstance().getClient().sendMessage("/app/game/" + gameId, currentPixel);
        }

    }

    private boolean wasRelease = false;
    public void drawOpponentInput(RankingDTO.PixelDTO pixelToUpdate) {
        if (!UserHolder.getInstance().getUser().getUsername().equals(drawer.getUsername())) {
            gc.setStroke(Color.web(pixelToUpdate.getDrawcolor()));
            gc.setLineCap(StrokeLineCap.ROUND);
            gc.setLineWidth(pixelToUpdate.thicknessToDouble());
            if (wasRelease) {
                gc.moveTo(pixelToUpdate.getxStartPosition(), pixelToUpdate.getyStartPosition());
            }
            gc.lineTo(pixelToUpdate.getxEndPosition(), pixelToUpdate.getyEndPosition());
            gc.stroke();
            gc.closePath();
            gc.beginPath();
            wasRelease = pixelToUpdate.getxEndPosition() == null && pixelToUpdate.getyEndPosition() == null;
        }
    }

    public void setDrawerWord(DrawerWord drawerWord) {
        this.drawerWordProperty().set(drawerWord.getWordToDraw().value);

    }

    public void revealWord(String correctWord) {
        this.drawerWordProperty().set(correctWord);
    }

    public boolean isOpenGameSceneProp() {
        return openGameSceneProp.get();
    }

    public BooleanProperty openGameScenePropProperty() {
        return openGameSceneProp;
    }

    public void setOpenGameSceneProp(boolean openGameSceneProp) {
        this.openGameSceneProp.set(openGameSceneProp);
    }

    public void setDrawer(User drawer) {
        this.drawer = drawer;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber.set(roundNumber);
    }

    public void setLengthOfWord(int lengthOfWord) {
        this.lengthOfWord.set(lengthOfWord);
    }

    public int getRoundNumber() {
        return roundNumber.get();
    }

    public IntegerProperty roundNumberProperty() {
        return roundNumber;
    }

    public int getLengthOfWord() {
        return lengthOfWord.get();
    }

    public IntegerProperty lengthOfWordProperty() {
        return lengthOfWord;
    }

    public String getDrawerWord() {
        return drawerWord.get();
    }

    public StringProperty drawerWordProperty() {
        return drawerWord;
    }

    public void updatePoints(Map<String, Integer> roundPoints) {
        for (User u : leaderboard) {
            int index = leaderboard.indexOf(u);
            int currentPoints = u.getScore();
            int newPoints = roundPoints.get(u.getUsername());
            u.setScore(currentPoints + newPoints);
            Platform.runLater(()->  leaderboard.set(index, u));
        }
    }

    public User getDrawer() {
        return drawer;
    }

    public ObservableList<RankingDTO> getRankingDTOS() {
        return rankingDTOS;
    }

    public void setRankingDTOS(List<RankingDTO> rankingDTOS) {
        this.rankingDTOS.addAll(rankingDTOS);
    }

    public boolean getGameEnded() {
        return gameEnded.get();
    }

    public BooleanProperty gameEndedProperty() {
        return gameEnded;
    }

    public String getUserGuessedCorrect() {
        return userGuessedCorrect.get();
    }

    public StringProperty userGuessedCorrectProperty() {
        return userGuessedCorrect;
    }

    public void setUserGuessedCorrect(String userGuessedCorrect) {
        this.userGuessedCorrect.set(userGuessedCorrect);
    }

    public ObservableList<Chatmessage> getChat() {
        return chat;
    }

    public void setChat(ObservableList<Chatmessage> chat) {
        this.chat = chat;
    }

    public void userGuessedWord(ChatActionResponse response) {
        Chatmessage message = new Chatmessage();
        message.senderId = response.getUsername();
        message.answer = response.getMessage();
        message.answerIsCorrect = response.isCorrect();
        chat.add(message);
    }


    public void clearCanvas() {
        gc.clearRect(0, 0, 900, 678);
    }
}

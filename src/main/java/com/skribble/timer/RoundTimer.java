package com.skribble.timer;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class RoundTimer{
    long currentTime;
    long counter = 0;
    long roundDuration;
    public boolean isStarted = false;
    public List<PropertyChangeListener> listeners = new ArrayList<>();
    private Timer timer = new Timer();

    public RoundTimer(long roundDuration, PropertyChangeListener e) {
        this.roundDuration = roundDuration + 1;
        listeners.add(e);
        currentTime = this.roundDuration;
        setState(currentTime);

    }
    public RoundTimer(long roundDuration) {
        this.roundDuration = roundDuration + 1;
    }


    public void start() {
        counter = 0;
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    counter++;
                    currentTime = roundDuration - counter;
                    setState(currentTime);
                }
            }, 0, 1000);


    }

    public void stop() {
            try {
                timer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try{Thread.sleep(500);}catch(InterruptedException e){e.printStackTrace();}
            counter = 0;

    }

    void notifyListeners(String property, long oldValue, long newValue) {
        listeners.forEach(l -> l.propertyChange(new PropertyChangeEvent(this, property, oldValue, newValue)));
    }

    void setState(long newTime) {
        notifyListeners("state", currentTime, currentTime = newTime);
    }

    private boolean isFinish() {
        return roundDuration == counter;
    }

}

package com.skribble;

import com.skribble.api.websocket.actionHandling.inputs.ChatAction;
import com.skribble.api.websocket.actionHandling.inputs.ClearCanvasAction;
import com.skribble.api.websocket.actionHandling.inputs.StartGameAction;
import com.skribble.api.websocket.actionHandling.response.RankingDTO;
import com.skribble.chat.Chatmessage;
import com.skribble.customListview.ChatCellFactory;
import com.skribble.drawingTools.Pencilweight;
import com.skribble.drawingTools.ToolType;
import com.skribble.holder.ClientHolder;
import com.skribble.api.websocket.SkribbleWebsocketClient;
import com.skribble.holder.SkribbleGameHolder;
import com.skribble.api.websocket.actionHandling.inputs.Action;
import com.skribble.customListview.LeaderboardCellFactory;
import com.skribble.utils.SceneChanger;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import com.skribble.timer.RoundTimer;
import com.skribble.user.User;
import com.skribble.holder.UserHolder;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SkribbleController {

    private SkribbleGame game;
    private RoundTimer timer;
    private PropertyChangeListener timerUpdate;
    private User currentUser;
    private SkribbleWebsocketClient client;
    private Stage dialog;
    @FXML
    private Text guessword;
    @FXML
    private Canvas drawingCanvas;
    @FXML
    private ColorPicker colorPicker;
    @FXML
    private ListView leaderboard;
    @FXML
    private Text roundTimer;
    @FXML
    private TextField chatInput;
    @FXML
    private ListView chatList;
    @FXML
    private ImageView pen;
    @FXML
    private ImageView eraser;
    @FXML
    private Circle klein;
    @FXML
    private Circle mittel;
    @FXML
    private Text roundNumber;
    @FXML
    private Circle groß;
    @FXML
    private AnchorPane anchor;
    @FXML
    private Button backbtn;

    @FXML
    protected void initialize() {
        game = SkribbleGameHolder.getInstance().getGame();
        game.setGc(drawingCanvas.getGraphicsContext2D());
        client = ClientHolder.getInstance().getClient();
        currentUser = UserHolder.getInstance().getUser();
        timerUpdate = (PropertyChangeEvent e) -> updateTimer((long) e.getNewValue());
        initListeners();
        leaderboard.setCellFactory(new LeaderboardCellFactory());
        chatList.setCellFactory(new ChatCellFactory());
        chatInput.setOnKeyPressed(keyEvent -> sendMessage(keyEvent));
        fillLeaderboard(game.getLeaderboard());
        try {
            if (UserHolder.getInstance().getUser().isHost()) {
                Thread.sleep(3000);
                client.sendMessage("/app/game/" + game.getGameId(),  new StartGameAction(3));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    private void initListeners() {
        pen.setOnMouseClicked(mouseEvent -> changeTool(ToolType.PEN));
        eraser.setOnMouseClicked(mouseEvent -> changeTool(ToolType.ERASER));
        colorPicker.setOnAction(event -> changeColor(event));
        klein.setOnMouseClicked(mouseEvent -> changeStrokeWidth(Pencilweight.KLEIN));
        mittel.setOnMouseClicked(mouseEvent -> changeStrokeWidth(Pencilweight.MITTEL));
        groß.setOnMouseClicked(mouseEvent -> changeStrokeWidth(Pencilweight.GROSS));
        backbtn.setOnMouseClicked(mouseEvent -> SceneChanger.change("home-view.fxml", anchor));
        drawingCanvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> game.draw(event));
        drawingCanvas.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> game.stopDraw(event));

        // Hier werden Variablen auf Änderungen überwacht.
        // Die Änderungen werden durch die verschiedenen Strategien bzw. durch die Websocket Aktionen erreicht.
        // Aufgrund der Änderung wird die View ebenfalls aktualisiert.
        game.getChat().addListener((ListChangeListener<Chatmessage>) change -> {
            change.next();
            Platform.runLater(() -> displayNewMessages(change.getAddedSubList()));
        });
        game.isHasGameStarted().addListener((observableValue, aBoolean, t1) -> toggleGame((ObservableValue<Boolean>) observableValue));
        game.gameEndedProperty().addListener((observableValue, aBoolean, t1) -> endGame((ObservableValue<Boolean>) observableValue));
        game.lengthOfWordProperty().addListener(((observableValue, number, t1) -> setLengthOfWord(t1.intValue())));
        game.roundNumberProperty().addListener((observableValue, number, t1) -> updateRoundNumber(t1.intValue()));
        game.drawerWordProperty().addListener((observableValue, string, t1) -> setDrawerWord(t1));
        game.userGuessedCorrectProperty().addListener((observableValue, aBoolean, t1) -> setDrawerWord(t1));
    }

    private void setDrawerWord(String word) {
        if (word != null ) {
            Platform.runLater(()->  this.guessword.setText(word));

        }
    }

    private void updateRoundNumber(int intValue) {
        roundNumber.setText("Runde " + intValue + " von 3");
    }

    private void setLengthOfWord(int number) {
        String placeholder = "";
        for (int i = 0; i < number; i++) {
            placeholder += "_ ";
        }
        if (isCurrentUserDrawing()) {
            guessword.setText(game.getDrawerWord());
        } else {
            guessword.setText(placeholder);
        }
    }

    private void toggleGame(ObservableValue<Boolean> observableValue) {

        if (observableValue.getValue() ) {
            System.out.println("Timer start");
            ClientHolder.getInstance().getClient().sendMessage("/app/game/" + game.getGameId(), new ClearCanvasAction());
            activateUI();
            announceDrawer();
            timer = new RoundTimer(60, timerUpdate);
            timer.start();
        } else  {
            System.out.println("Timer stop");
            timer.stop();
        }
    }

    private void announceDrawer() {
        Chatmessage message = new Chatmessage();
        message.senderId = "Game Master";
        message.answer = game.getDrawer().getUsername() + " malt gerade!";
        message.answerIsCorrect = false;
        game.getChat().add(message);
    }

    private void updateTimer(long time) {
        Platform.runLater(() -> {
            roundTimer.setText(time + " s");
        });
    }

    private void activateUI() {
        if (isCurrentUserDrawing()) {
            activateCanvas(false);
            activateChat(true);
        } else {
            activateCanvas(true);
            activateChat(false);
        }
    }

    private void activateCanvas(boolean activate) {
        drawingCanvas.setDisable(activate);
    }

    private void activateChat(boolean activate) {
        chatInput.setDisable(activate);
    }

    private boolean isCurrentUserDrawing() {
        return currentUser.getUsername().equals(game.getDrawer().getUsername());
    }

    private void changeTool(ToolType type) {
            updateToolsUi(type);
            game.getTool().setType(type);
            game.getTool().setColor(colorPicker.getValue());
    }

    private void updateToolsUi(ToolType type) {
        if (String.valueOf(type).equals("PEN")) {
            eraser.setOpacity(0.5);
            pen.setOpacity(1);
        } else if(String.valueOf(type).equals("ERASER")) {
            eraser.setOpacity(1);
            pen.setOpacity(0.5);
        }
    }

    private void sendMessage(KeyEvent keyEvent) {
        String input = chatInput.getText().trim();
        if (keyEvent.getCode().equals(KeyCode.ENTER) && input.length() > 0) {
            Action action = new ChatAction(input);
            ClientHolder.getInstance().getClient().sendMessage("/app/game/" + game.getGameId(), action);
            chatInput.setText("");
        }
    }

    private void displayNewMessages(List<? extends Chatmessage> list) {
        for (Chatmessage m : list) {
            chatList.getItems().add(m);
        }
    }

    private void fillLeaderboard(List<? extends User> addedSubList) {
        leaderboard.setItems(game.getLeaderboard());
    }

    private void changeColor(ActionEvent event) {
        game.getTool().setColor(colorPicker.getValue());
    }
    private void changeStrokeWidth(Pencilweight pencilweight) {
        ArrayList<Circle> weights = new ArrayList<>();
        weights.add(klein);
        weights.add(mittel);
        weights.add(groß);
        for(Circle c : weights) {
            c.setOpacity(0.5);
        }
        if (pencilweight == Pencilweight.KLEIN) {
            this.klein.setOpacity(1);
        } else if (pencilweight == Pencilweight.MITTEL) {
            this.mittel.setOpacity(1);
        } else if (pencilweight == Pencilweight.GROSS) {
            this.groß.setOpacity(1);
        }
        game.getTool().setThickness(pencilweight);
    }

    private void endGame(ObservableValue<Boolean> observableValue) {
        if (observableValue.getValue()) {
            createPopup(game.getRankingDTOS());
        }
    }
    private void createPopup(List<RankingDTO> ranking) {
        Platform.runLater(() -> {
            Stage thisStage = (Stage) pen.getScene().getWindow();
            dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(thisStage);
            VBox dialogVbox = new VBox(20);
            ranking.sort((o1, o2) -> o2.rank - o1.rank);
            ListView listView = new ListView();
            for (RankingDTO rank: ranking) {
                listView.getItems().add(rank.rank + ". " + rank.gameUserDTO.getUsername() + " (" + rank.score + ")");
            }
            dialogVbox.getChildren().add(listView);
            Scene dialogScene = new Scene(dialogVbox, 300, 200);
            dialog.setScene(dialogScene);
            dialog.setOnCloseRequest(windowEvent -> SceneChanger.change("home-view.fxml", anchor));
            dialog.show();
        });
    }

}